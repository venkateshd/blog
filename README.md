[![Appveyor Build Status](https://ci.appveyor.com/api/projects/status/eg7hij8sd58a1tfi?svg=true&retina=true)](https://ci.appveyor.com/project/venkateshdi/blog-4uikl)
[![GitLab CI Build Status](https://gitlab.com/venkateshd/blog/badges/master/pipeline.svg)](https://gitlab.com/venkateshd/blog/commits/master)

What?
====
This project was created to try GitLab Pages with static content created using Wyam static site generator.

How?
====

*Appveyor*
    
Appveyor used to extract ssg tool wyam.io and generate the blog artifacts available in input folder. Once the static site content is ready appveyor commits the ontent in to output folder.

*Gitlab CI*

Once appveyor commits the site content to output folder gitlab CI picks up the content and starts deployemnt. This was handled by pages job.